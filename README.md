CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Troubleshooting
 * Project members


INTRODUCTION
------------

Cloudflare Stream Sync is a module that extends the functionality provided by
the Cloudflare Stream module.

This module imports all videos from the Cloudflare Stream site that aren't 
present on the Drupal site.


REQUIREMENTS
------------

A configured Cloudflare Stream module & the hosted_video media type.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Start the sync process manually via the Cloudflare Stream Sync settings
on Administration » Config » Media » Cloudflare Stream Sync.


USAGE
-------------

Enable this module. Make sure the hosted_video media type is enabled.
Head to Administration » Config » Media » Cloudflare Stream Sync & hit the
sync button.

Optionally, you can set a server cron that triggers the drush css process to 
automate the file import.


TROUBLESHOOTING
-------------

* If there is an unexpected issue, please check the issue queue:
  https://www.drupal.org/project/issues/cloudflare_stream

* If there is an undocumented issue, please create one:
  https://www.drupal.org/node/add/project-issue/cloudflare_stream


PROJECT MEMBERS
-----------

See the list of current project members on
https://git.drupalcode.org/project/cloudflare_stream_sync/-/project_members
