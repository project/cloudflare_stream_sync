<?php

namespace Drupal\cloudflare_stream_sync;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface;
use Drupal\cloudflare_stream\Service\CloudflareStreamInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\media\Entity\Media;

/**
 * Sync videos service.
 *
 * @package Drupal\cloudflare_sync
 */
class SyncVideos {

  /**
   * The Cloudflare Stream Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Cloudflare Stream service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamInterface
   */
  protected $cloudflareStream;

  /**
   * The Cloudflare Stream API service.
   *
   * @var \Drupal\cloudflare_stream\Service\CloudflareStreamApiInterface
   */
  protected $cloudflareStreamApi;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    CloudflareStreamInterface $cloudflareStream,
    CloudflareStreamApiInterface $cloudflareStreamApi
  ) {
    $this->config = $config_factory->getEditable('cloudflare_stream_sync.settings');
    $this->dateFormatter = $date_formatter;
    $this->cloudflareStream = $cloudflareStream;
    $this->cloudflareStreamApi = $cloudflareStreamApi;
  }

  public function fetchVideos() {
    $date = NULL;
    // Add last imported timestamp to uri
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'custom', 'Y-m-d\TH:m:s\Z', 'UTC');
    }

    $response = $this->cloudflareStreamApi->listVideos($date);

    return $response['result'];
  }

  /**
   * Sync the external videos.
   *
   * @throws \Exception
   */
  public function syncVideos() {
    // Fetch the new videos.
    $video_list = $this->fetchVideos();
    if (empty($video_list)) {
      return;
    }

    // Process all videos.
    foreach($video_list as $video) {
      SyncVideos::processVideo($video);
    }

    // Update last imported timestamp.
    $this->config->set('last_imported', time())->save();
  }

  /**
   * Sync the external videos.
   *
   * @throws \Exception
   */
  public static function processVideo($video) {
    //videoID.
    $videoID = SyncVideos::getVideoID($video);

    if (!SyncVideos::checkIfVideoIDExists($videoID)) {
      //filename.
      $filename = SyncVideos::getFilename($video);
      // Create file
      $file = file_save_data('', 'public://' . $filename);

      // Create media object of file
      $media = Media::create([
        'bundle' => 'hosted_video',
        'uid' => \Drupal::currentUser()->id(),
        'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
        'name' => $filename,
        'field_media_hosted_video' => [
          'target_id' => $file->id(),
          'cloudflareStreamVideoID' => $videoID,
          'thumbnail' => SyncVideos::getThumbnail($video),
        ],
      ]);
      $media->setPublished(TRUE);
      $media->save();

      // Register file usages in the DB
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'file', 'media', $media->id());
      $file_usage->add($file, 'cloudflare_stream', 'cloudflarevideo', \Drupal::currentUser()
        ->id());
      $file->setPermanent();
      $file->save();

      return $filename;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Batch API functions
   *
   * Helper function to sync the video.
   */
  public static function syncVideoCallback($video, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
    }

    if ($filename = SyncVideos::processVideo($video)) {
      $context['sandbox']['progress']++;
      $context['results'][] = $filename;
      $context['message'] = t('Syncing video @filename', [
        '@filename' => $filename,
      ]);
    }
  }

  /**
   * Batch API functions
   *
   * Finished batch callback.
   */
  public static function finishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();

    if ($success) {
      // Set last imported timestamp
      $config = \Drupal::service('config.factory')
        ->getEditable('cloudflare_stream_sync.settings');
      $config->set('last_imported', time())->save();

      $message = \Drupal::translation()->formatPlural(
        count($results),
        t("One video processed."), t("@count videos processed.")
      );
      $messenger->addMessage($message);
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t("An error occurred while processing @operation with arguments : @args",
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  private static function getFilename($video) {
    return $video['meta']['name'];
  }

  private static function getVideoID($video) {
    return $video['uid'];
  }

  private static function getThumbnail($video) {
    return $video['thumbnail'];
  }

  /**
   * Helper function to check if VideoID already exists.
   */
  private static function checkIfVideoIDExists($videoID) {
    $query = \Drupal::database()
      ->select('media__field_media_hosted_video', 'media_cv');
    $query->fields('media_cv');
    $query->condition('field_media_hosted_video_cloudflareStreamVideoID', $videoID);
    $results = $query->execute()->fetchAll();

    if (count($results) === 0) {
      return FALSE;
    }

    return TRUE;
  }
}
