<?php

namespace Drupal\cloudflare_stream_sync\Commands;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\cloudflare_stream_sync\SyncVideos;
use Drush\Commands\DrushCommands;

/**
 * Sync Drush Command.
 */
class SyncCommand extends DrushCommands {

  /**
   * The actual sync process.
   *
   * @var \Drupal\cloudflare_stream_sync\SyncVideos
   */
  protected $sync;

  /**
   * SyncCommand constructor.
   *
   * @param \Drupal\cloudflare_stream_sync\SyncVideos $sync_videos
   *   Synchronizer.
   */

  public function __construct(
    SyncVideos $sync_videos
  ) {
    $this->sync = $sync_videos;
  }

  /**
   * Perform the Cloudflare Stream video sync.
   *
   * @command cloudflarestream:sync
   * @aliases css
   * @usage cloudflarestream:sync
   */
  public function import() {
    try {
      $this->sync->syncVideos();
    }
    catch (\Exception $exception) {

      $message = new FormattableMarkup(
        '@class (@function): sync error. Error details are as follows:<pre>@response</pre>',
        [
          '@class' => get_class(),
          '@function' => __FUNCTION__,
          '@response' => print_r($exception->getMessage(), TRUE),
        ]
      );

      // Log the error.
      watchdog_exception('cloudflare_stream_sync', $exception, $message);
    }
  }

}
