<?php

namespace Drupal\cloudflare_stream_sync\Form;

use Drupal\cloudflare_stream_sync\SyncVideos;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures the settings for Cloudflare Stream Sync module.
 */
class SyncVideosSettings extends FormBase {

  /**
   * The Cloudflare Stream Sync config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The videos sync service.
   *
   * @var \Drupal\cloudflare_stream_sync\SyncVideos
   */
  protected $syncVideos;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    SyncVideos $sync_videos
  ) {
    $this->config = $config_factory->get('cloudflare_stream_sync.settings');
    $this->dateFormatter = $date_formatter;
    $this->syncVideos = $sync_videos;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('cloudflare_stream_sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_stream_sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Explanation.
    $form['explanation'] = [
      '#markup' => t("<p>The <strong>Cloudflare Stream Sync</strong> module creates a media item for each video item that is on the Cloudflare stream platform.</p>
<ul><li>The <strong>initial sync</strong> will sync all videos present on Cloudflare.</br>
<p>During that sync, we check whether the video exists as a media item on the website.</br>
If it doesn't, it will be imported.</p></li>
<li>The <strong>next syncs</strong> will only import new videos that were added to the Cloudflare Stream platform <strong>after</strong> the last sync date.</li></ul>"),
    ];

    // Show last imported timestamp as date eg. 2014-01-02T02:20:00Z
    // Set last imported timestamp.
    $timestamp = $this->config->get('last_imported');
    if (isset($timestamp)) {
      $date = $this->dateFormatter->format($timestamp, 'short');
      $form['last_imported_timestamp'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("<strong>Last import:</strong> @date", ['@date' => $date]),
      ];
    }

    // Submit button.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync videos'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Initialize operations
    $operations = [];

    $videos = $this->syncVideos->fetchVideos();
    if (count($videos) >= 1) {
      foreach ($videos as $video) {
        $operations[] = [
          'Drupal\cloudflare_stream_sync\SyncVideos::syncVideoCallback',
          [$video],
        ];
      }

      $batch = [
        'title' => $this->t("Synchronisation process"),
        'init_message' => $this->t("The sync process is starting."),
        'progress_message' => $this->t("Processed @current out of @total."),
        'error_message' => $this->t("The sync has encountered an error."),
        'operations' => $operations,
        'finished' => [
          'Drupal\cloudflare_stream_sync\SyncVideos',
          'finishedCallback',
        ],
      ];

      batch_set($batch);
    }
    else {
      $this->messenger()->addMessage($this->t('Nothing to sync.'));
    }
  }

}
